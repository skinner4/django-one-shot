from django.shortcuts import get_object_or_404, render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoList, TodoListForm, TodoItemForm, TodoItemCreate

# Create your views here.

def todo_list_list(request):
   todos = TodoList.objects.all()
   context = {
      "todolist_objects": todos
   }
   return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    #recipe = Recipe.objects.get(id=id)

    todo_list = get_object_or_404(TodoList, id=id)

    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    edit_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance = edit_list)
        if form.is_valid():
            edit_list = form.save()
            return redirect("todo_list_detail", id=edit_list.id)
    else:
        form = TodoListForm(instance = edit_list)
    context = {
        "form": form,
        }
    return render (request, "todos/edit.html", context)

def todo_list_delete(request, id):
  todolist = TodoList.objects.get(id=id)
  if request.method == "POST":
    todolist.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemCreate(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemCreate()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def todo_item_update(request, id):
    edit_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=edit_item)
        if form.is_valid():
            edit_item = form.save()
            return redirect("todo_list_detail", id=edit_item.id)
    else:
        form = TodoItemForm(instance=edit_item)
    context = {
        "form": form,
        "edit_item": edit_item
        }
    return render (request, "todos/edit_item.html", context)
